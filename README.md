# template-files

Group-level templates for issues, merge requests, etc.

See [the GitLab documentation](https://docs.gitlab.com/ee/user/project/description_templates.html#set-group-level-description-templates) for details.
